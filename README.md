# A2Z Grocery

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/cohort-6-final-project-group-12/a2z-grocery.git
git branch -M main
git push -uf origin main
```

***

## Name : A2Z Grocery

## Description

The BackEnd Link : https://gitlab.com/cohort-6-final-project-group-12/a2z-grocery-backend

A webbased graocery store appilication where we mainly have 2 modules where we can perform the following activites in each module
\
User    : Login/Register, Browser Products, Add To Cart, Remove From Cart, Pay  
\
Admin   : Login, Browser/Remove/Add Products, Browser/Remove/Add Product Categories

## Installation
Clone the repo\
npm install (to install node modules)\
npm i ng2-search-filter --save\
ng-image-slider --force\
npm install --save sweetalert2\
npm install ngx-toastr --save\
npm install @angular/animations --save\
npm i @abacritt/angularx-social-login\
npm start (to start the appilication)

## Files Present
headers     :   mainheader, userheader, adminheader\
pipes       :   sum, search(done via ng2PipeFilter installed)\
services    :   loginService, productService, cartService\
components  :   mainpage, register, login, user, updateuser, cart, admindashboard,
                addProduct, updateProduct, productCategories, addPc, updatePC

## Currently Available Features (Apart from above operations)
Password Encryption (In the backend done via Bcrypt Password Encoder )\
Sweet alerts\
Toastr Messages\
Validations\
Payment Gateway (Done via Razorpay API)

## Roadmap ( Future Scope )
To Add Google Authentication (Code is present in frontend need to connect with backend)\
To Add Email/Otp Services on payments

## Project status
Everything is working fine till what is done wihtout any issues