import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddpcComponent } from './addpc/addpc.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { CartComponent } from './cart/cart.component';
import { LoginComponent } from './login/login.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { ProductcategoriesComponent } from './productcategories/productcategories.component';
import { RegisterComponent } from './register/register.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { UpdatepcComponent } from './updatepc/updatepc.component';
import { UpdateproductComponent } from './updateproduct/updateproduct.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {path:"",component:MainpageComponent},
  {path:"login",component:LoginComponent},
  {path:"register",component:RegisterComponent},
  {path:"user",component:UserComponent},
  {path:"admin",component:AdmindashboardComponent},
  {path:"addProduct",component:AddproductComponent},
  {path:"updateProduct",component:UpdateproductComponent},
  {path:"cart",component:CartComponent},
  {path:"productCategories",component:ProductcategoriesComponent},
  {path:"addPC",component:AddpcComponent},
  {path:"updatePC",component:UpdatepcComponent},
  {path:"updateUser",component:UpdateUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
