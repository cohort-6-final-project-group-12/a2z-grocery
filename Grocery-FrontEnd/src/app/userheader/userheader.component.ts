import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'app/login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-userheader',
  templateUrl: './userheader.component.html',
  styleUrls: ['./userheader.component.css']
})
export class UserheaderComponent
{
  User:any=[];

  constructor(private loginService:LoginService,private route:Router) 
  {
    this.User=this.loginService.User;
  }

  updateUser()
  {
    this.route.navigateByUrl("updateUser");
  }

  userHome()
  {
    this.route.navigateByUrl("user");
  }

  userLogout()
  {
    this.route.navigateByUrl("");
    Swal.fire(
      'Log Out',
      'Successful !!',
      'success'
    )
  }
  cart()
  {
    this.route.navigateByUrl("cart");
  }
}
