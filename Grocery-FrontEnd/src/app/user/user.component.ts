import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'app/cart.service';
import { LoginService } from 'app/login.service';
import { ProductService } from 'app/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent 
{
  title = 'Welcome ' + this.loginSerice.User.name;
  searchText = '';
  Products: any[] = [];
  ProductCategory: any[] = [];
  item:any=[];

  ngOnInit(): void {
    this.getProducts();
    this.getProductCategory();
  }

  constructor
  (
    private route: Router,
    private loginSerice: LoginService,
    private productService: ProductService,
    private cartService:CartService,
  ){}

  cart() {
    this.route.navigateByUrl("cart");
  }

  getProducts() {
    return this.productService.getProducts().subscribe((data: any) => {
      console.log("collecting data");
      console.log(data);
      this.Products = Object.values(data);
    });
  }

  getProductCategory() {
    return this.productService.getProductCategory().subscribe((data: any) => {
      console.log(data);
      this.ProductCategory = Object.values(data);
    });
  }

  getProductByPcId(pcid: any) {
    return this.productService.getProductByPcId(pcid).subscribe((data: any) => {
      console.log("P By PcId" + data);
      this.Products = Object.values(data);
    });
  }

  addToCart(product:any)
  {
    this.item={
      uid: this.loginSerice.User.id,
      pid: product.pid,
      pprice: product.pprice,
      pimage: product.pimage,
      pname: product.pname
    };
    console.log(this.item);
    return this.cartService.addtoCart(this.item).subscribe((data:any)=>{
      console.log(data);
      Swal.fire(
        'Product Added',
        'Successfully !!',
        'success'
      )
    });
  }

}

