import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-adminheader',
  templateUrl: './adminheader.component.html',
  styleUrls: ['./adminheader.component.css']
})
export class AdminheaderComponent 
{
  constructor(private route:Router) {}

  adminDash()
  {
    this.route.navigateByUrl("admin");
  }

  manageCat()
  {
    this.route.navigateByUrl("productCategories");
  }

  adminLogout()
  {
    this.route.navigateByUrl("");
    Swal.fire(
      'Log Out',
      'Successful !!',
      'success'
    )
  }
}
