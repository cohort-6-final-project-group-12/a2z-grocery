import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent {

  constructor(private route:Router)
  {

  }
  // 6 images
  imageObject = [
    {
      image: 'https://www.supermarketnews.com/sites/supermarketnews.com/files/styles/article_featured_retina/public/Home-food-delivery%20web.jpg?itok=RwRIjV4y',
      thumbImage: 'https://www.supermarketnews.com/sites/supermarketnews.com/files/styles/article_featured_retina/public/Home-food-delivery%20web.jpg?itok=RwRIjV4y',
      title: 'On Time'
    },
    
    {
      image: 'https://cdn.winsightmedia.com/platform/files/public/2022-12/background/Food%20price%20inflation-grocery%20basket-supermarket%20receipt_Shutterstock.jpg?VersionId=PykF1G4r6zHo1zAebD5UNp9WEoAuEW7v',
      thumbImage: 'https://cdn.winsightmedia.com/platform/files/public/2022-12/background/Food%20price%20inflation-grocery%20basket-supermarket%20receipt_Shutterstock.jpg?VersionId=PykF1G4r6zHo1zAebD5UNp9WEoAuEW7v',
      title: 'In Budget'
    },
    
    {
      image: 'https://media.istockphoto.com/id/1216828053/photo/shopping-basket-with-fresh-food-grocery-supermarket-food-and-eats-online-buying-and-delivery.jpg?s=170667a&w=0&k=20&c=YST4_YPiYcdG-1YTz4t3HP10gjV80Dqt3pH7eWb6Wk0=',
      thumbImage: 'https://media.istockphoto.com/id/1216828053/photo/shopping-basket-with-fresh-food-grocery-supermarket-food-and-eats-online-buying-and-delivery.jpg?s=170667a&w=0&k=20&c=YST4_YPiYcdG-1YTz4t3HP10gjV80Dqt3pH7eWb6Wk0=',
      title: '100% Satisfaction'
    },
    
    {
      image: 'https://thumbs.dreamstime.com/b/grocery-online-shop-to-order-food-delivery-supermarket-woman-hands-using-laptop-computer-shopping-store-electronic-177445014.jpg',
      thumbImage: 'https://thumbs.dreamstime.com/b/grocery-online-shop-to-order-food-delivery-supermarket-woman-hands-using-laptop-computer-shopping-store-electronic-177445014.jpg',
      title: 'Order From Anywhere'
    },
    
    {
      image: 'https://images.hindustantimes.com/img/2021/12/23/1600x900/online-payment-shutterstock_dee0e9ae-b532-11e6-b935-511f3378ef5e_1640222960136.jpg',
      thumbImage: 'https://images.hindustantimes.com/img/2021/12/23/1600x900/online-payment-shutterstock_dee0e9ae-b532-11e6-b935-511f3378ef5e_1640222960136.jpg',
      title: '₹ Pay Online'
    },
    
    {
      image: 'https://previews.123rf.com/images/magurok/magurok1610/magurok161000031/64043745-hand-tapping-on-coin-on-smartphone-screen-gold-coins-falling-to-wallet-earn-money-online-pay-per.jpg',
      thumbImage: 'https://previews.123rf.com/images/magurok/magurok1610/magurok161000031/64043745-hand-tapping-on-coin-on-smartphone-screen-gold-coins-falling-to-wallet-earn-money-online-pay-per.jpg',
      title: 'Earn Back On Each Purchase'
    }
  ];

  register()
  {
    this.route.navigateByUrl("register");
  }
  
  login()
  {
    this.route.navigateByUrl("login");
  }
}
