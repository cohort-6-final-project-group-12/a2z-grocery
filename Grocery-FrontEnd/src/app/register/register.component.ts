import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  constructor(private route:Router, private loginService:LoginService)
  {

  }

  registerUser(registerForm:any)
  { 
    this.loginService.findUserByEmail(registerForm.email).subscribe((data:any)=>{
      
      if(data)
      {
        alert("User Exists with Email, Please login to your account");
        this.route.navigateByUrl("login");
      }
      else
      {
        console.log("adding new user");
        this.loginService.addUser(registerForm).subscribe((data:any)=>{
          console.log(data);
          alert("Registration Successful, Please login to your account");
          this.route.navigateByUrl("login");
        });
      }

    });
  }

  register()
  {
    this.route.navigateByUrl("register");
  }
  
  login()
  {
    this.route.navigateByUrl("login");
  }
}