import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-addpc',
  templateUrl: './addpc.component.html',
  styleUrls: ['./addpc.component.css']
})
export class AddpcComponent 
{
  pc:any;

  constructor(private route:Router, private productService:ProductService) {}
  
  ngOnInit(): void
  {
    this.pc=this.productService.pc
  }

  add(addPC:any)
  {
    console.log(addPC);

    this.productService.addPC(addPC).subscribe((data:any)=>{
      console.log(data);
    });

    this.route.navigateByUrl("productCategories");
  }
}
