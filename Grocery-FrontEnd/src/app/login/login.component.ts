import { SocialAuthService } from '@abacritt/angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit
{    
  User:any;
  alpha:any;
  final:any;
  
  user:any;
  loggedIn:any;
  constructor(private authService: SocialAuthService,private route:Router, private loginService:LoginService,private toastr: ToastrService)
  {

  }

  loginUser(loginForm:any)
  {
    console.log(loginForm);
    if(loginForm.emailId=="admin" && loginForm.password=="admin")
    {
      if(loginForm.typedcapt==this.final)
      {
        console.log("in-admin");
        this.toastr.success('Login Successful !!');
        this.route.navigateByUrl("admin");
      }
      else
      {
        alert("Enter valid captcha");
      }
    }
    else
    {
      this.userCheck(loginForm);
    }

  }

  ngOnInit(): void 
  {
    this.captcha();
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      console.log(this.user.id);
    });
  }

  captcha()
  { this.alpha=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
                  ,'a','c','b','d','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','v','n','m','1','2','3','4','5','6','7','8','9','0']
    const a=this.alpha[Math.floor(Math.random()*62)];
    const b=this.alpha[Math.floor(Math.random()*62)];
    const c=this.alpha[Math.floor(Math.random()*62)];
    const d=this.alpha[Math.floor(Math.random()*62)];
    const e=this.alpha[Math.floor(Math.random()*62)];
    const f=this.alpha[Math.floor(Math.random()*62)];
  
    this.final=a+b+c+d+e+f;
    return this.final;
  }

  register()
  {
    this.route.navigateByUrl("register");
  }

  login()
  {
    this.route.navigateByUrl("login");
  }

  userCheck(loginObj:any)
  {
    this.loginService.findUserByEmail(loginObj.emailId).subscribe((data:any)=>{
      
      if(data==null)
      {
        alert("No Account Exists, Please register first");
        this.route.navigateByUrl("register");
      }
      else
      {
        this.loginService.login(loginObj).subscribe((data:any)=>{

          this.User=data;
          this.loginService.User=data;
          console.log(this.User);
          
          if(data)
          {
            if(loginObj.typedcapt==this.final)
            {
              console.log("in-user");
              this.toastr.success('Login Successful !!');
              this.route.navigateByUrl("user");
            }
            else
            {
              alert("Enter valid captcha");
            }
          }
          else
          {
            alert("Invalid Credentials");
          }
    
        });
      }
    });
  }
}