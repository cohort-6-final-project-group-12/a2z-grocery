import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-updatepc',
  templateUrl: './updatepc.component.html',
  styleUrls: ['./updatepc.component.css']
})
export class UpdatepcComponent implements OnInit
{

  pc:any;

  constructor(private route:Router, private productService:ProductService) {}
  
  ngOnInit(): void
  {
    this.pc=this.productService.pc
  }

  update(updatePC:any)
  {
    console.log(updatePC);

    this.productService.updatePC(updatePC).subscribe((data:any)=>{
      console.log(data);
    });

    this.route.navigateByUrl("admin");
  }
}
