import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService
{
  Product:any[]=[];
  pc:any[]=[];
  

  constructor(private httpclient:HttpClient) {}

  getProducts() 
  {
    return this.httpclient.get("/displayP")
  }
  
  addProducts(p: any)
  {
    return this.httpclient.post("/displayP", p);
  }
  
  updateProduct(p: any)
  {
    return this.httpclient.put("/displayP", p);
  }
  
  deleteProduct(pid: any)
  {
    return this.httpclient.delete("/displayP/" + pid);
  }

  getProductCategory()
  {
    return this.httpclient.get("/displayPC/");
  } 
  getProductByPId(pid:any)
  {
    return this.httpclient.get("/Product/"+pid);
  } 
  getProductByPcId(pcid:any)
  {
    return this.httpclient.get("/displayP/"+pcid);
  } 

  addPC(pc:any)
  {
    return this.httpclient.post("/displayPC",pc);
  }

  updatePC(pc:any)
  {
    return this.httpclient.put("/displayPC",pc);
  }
}
