import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  User:any=[];

  constructor(private httpclient: HttpClient) {}

  login(loginObj: any)
  {
    return this.httpclient.get('/login/'+loginObj.emailId+'/'+loginObj.password);
  }

  findUserByEmail(email: any)
  {
    return this.httpclient.get('/displayUser/'+email);
  }

  getUsers() {
    return this.httpclient.get("/displayUser");
  }
  addUser(u: any) {
    return this.httpclient.post("/displayUser", u);
  }
  updateUser(u: any) {
    return this.httpclient.put("/displayUser", u);
  }
}
