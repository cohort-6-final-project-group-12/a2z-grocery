import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mainheader',
  templateUrl: './mainheader.component.html',
  styleUrls: ['./mainheader.component.css']
})
export class MainheaderComponent 
{
  constructor(private route:Router){}
  
  register()
  {
    this.route.navigateByUrl("register");
  }

  login()
  {
    this.route.navigateByUrl("login");
  }
}
