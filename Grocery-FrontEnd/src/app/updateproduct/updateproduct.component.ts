import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-updateproduct',
  templateUrl: './updateproduct.component.html',
  styleUrls: ['./updateproduct.component.css']
})
export class UpdateproductComponent 
{
  Product:any;
  ProductCategory:any[]=[];

  constructor(private productService:ProductService, private route:Router)
  {

  }

  ngOnInit(): void
  {
    this.Product=this.productService.Product;
    console.log(this.Product);
    this.getProductCategory();
  }

  update(updateProduct:any)
  {
    console.log(updateProduct);

    this.productService.updateProduct(updateProduct).subscribe((data:any)=>{
      console.log(data);
    });

    this.route.navigateByUrl("admin");
  }

  getProductCategory()
  {
    return this.productService.getProductCategory().subscribe((data:any)=>{
      console.log(data);
      this.ProductCategory=Object.values(data);
    });
  }

}
