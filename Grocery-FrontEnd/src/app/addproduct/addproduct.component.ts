import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit
{
  ProductCategory:any[]=[];

  constructor(private route:Router, private productService:ProductService)
  {

  }
  ngOnInit(): void 
  {
    this.getProductCategory();
  }

  add(addProduct:any)
  {
    console.log(addProduct);

    this.productService.addProducts(addProduct).subscribe((data:any)=>{
      console.log(data);
    });

    this.route.navigateByUrl("admin");
  }

  getProductCategory()
  {
    return this.productService.getProductCategory().subscribe((data:any)=>{
      console.log(data);
      this.ProductCategory=Object.values(data);
    });
  }
}
