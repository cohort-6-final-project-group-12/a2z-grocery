import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent implements OnInit{
  title = 'Admin DashBoard';
  searchText='';
  
  Products:any[]=[];
  ProductCategory:any[]=[];

  constructor(private route:Router, private productService:ProductService) 
  {
    this.getProducts();
  }
  
  ngOnInit(): void
  {
    this.getProductCategory();
  }

  getProducts()
  {
    return this.productService.getProducts().subscribe((data:any)=>{
      console.log("collecting data");
      console.log(data);
      this.Products=Object.values(data);
    });
  }

  getProductCategory()
  {
    return this.productService.getProductCategory().subscribe((data:any)=>{
      console.log(data);
      this.ProductCategory=Object.values(data);
    });
  }

  getProductByPcId(pcid:any)
  {
    return this.productService.getProductByPcId(pcid).subscribe((data:any)=>{
      console.log("P By PcId"+data);
      this.Products=Object.values(data);
    });
  }

  addProduct()
  {
    this.route.navigateByUrl("addProduct");
  }

  updateProduct(product:any)
  {
    this.productService.Product=product;
    this.route.navigateByUrl("updateProduct")
  }

  deleteProduct(pid:any)
  {
    if(confirm("Are you sure to delete"))
    {
      this.productService.deleteProduct(pid).subscribe(()=>{
        this.getProducts();
      });
    }
  }
}
