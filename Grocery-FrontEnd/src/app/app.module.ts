import { SocialLoginModule, SocialAuthServiceConfig, GoogleSigninButtonModule } from '@abacritt/angularx-social-login';
import {GoogleLoginProvider,} from '@abacritt/angularx-social-login';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { NgImageSliderModule } from 'ng-image-slider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { UpdateproductComponent } from './updateproduct/updateproduct.component';
import { ProductcategoriesComponent } from './productcategories/productcategories.component';
import { AdminheaderComponent } from './adminheader/adminheader.component';
import { AddpcComponent } from './addpc/addpc.component';
import { UpdatepcComponent } from './updatepc/updatepc.component';
import { UserheaderComponent } from './userheader/userheader.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { MainheaderComponent } from './mainheader/mainheader.component';
import { CartComponent } from './cart/cart.component';
import { SumPipe } from './sum.pipe';
import { FooterComponent } from './footer/footer.component';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    MainpageComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    AdmindashboardComponent,
    AddproductComponent,
    UpdateproductComponent,
    ProductcategoriesComponent,
    AdminheaderComponent,
    AddpcComponent,
    UpdatepcComponent,
    UserheaderComponent,
    UpdateUserComponent,
    MainheaderComponent,
    CartComponent,
    SumPipe,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgImageSliderModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    SocialLoginModule,
    GoogleSigninButtonModule,
    ToastrModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '706306955268-ecg457v63lkl13midfvr06s30pqtt9ae.apps.googleusercontent.com'
            )
          },
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
