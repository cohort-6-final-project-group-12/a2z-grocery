import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'app/login.service';
import { OrderService } from 'app/order.service';
import { CartService } from '../cart.service';

declare var Razorpay: any;
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartItems:any=[];
  sum=0;
  User:any=[];
  handler: any = null;

  form: any = {}; 
  paymentId: any;
  error: any;

  constructor(private route:Router,private loginService: LoginService, private cartService: CartService,private orderService:OrderService) {}


  ngOnInit(): void {
    this.User=this.loginService.User;
    this.getCartItemsByUid(this.User.id);
  }
  
  getCartItemsByUid(uid: any) {
    return this.cartService.displayCartByUiD(uid).subscribe((data: any) => {
      console.log(data);
      this.cartItems = Object.values(data);
      this.sum=0;
      this.cartItems.forEach((a: { pprice: number; }) => this.sum += a.pprice);
      this.sum=this.sum-this.User.token;
      console.log(this.sum);
    });
  }
  
  deleteItem(cid: any) {
    if (confirm("Are you sure to delete")) {
      this.cartService.deleteiteminCart(cid).subscribe(() => {
        this.ngOnInit();
      });
    }
  }

  checkout()
  {
    this.cartService.deleteiteminCartByUid(this.loginService.User.id).subscribe(() => {
      this.ngOnInit();
    });
    this.User.token=Math.round(this.sum/100);
    console.log(this.User);
    this.loginService.updateUser(this.User).subscribe(()=>{
      this.ngOnInit();
    });

    this.onSubmit();
    this.route.navigateByUrl("user");
  }

  options = {
    "key": "rzp_test_JEV5T9voaX0yzl",
    "amount": "105", 
    "name": "A2ZGroccery",
    "description": "Online Groccery Store",
    "image": "https://www.javachinna.com/wp-content/uploads/2020/02/android-chrome-512x512-1.png",
    "order_id":"",
    "handler": function (response:any){
      var event = new CustomEvent("payment.success", 
        {
          detail: response,
          bubbles: true,
          cancelable: true
        }
      );	  
      window.dispatchEvent(event);
    }
    ,
    "prefill": {
    "name": "",
    "email": "",
    "contact": ""
    },
    "notes": {
    "address": ""
    },
    "theme": {
    "color": "#3399cc"
    }
    };

    onSubmit(): void {
      this.paymentId = ''; 
      this.error = ''; 
      this.form={
        name: this.User.name,
        email: this.User.email,
        phone: this.User.phone,
        amount: this.sum,
      };
      this.orderService.createOrder(this.form).subscribe(
      data => {
        this.options.key = data.secretId;
        this.options.order_id = data.razorpayOrderId;
        this.options.amount = data.applicationFee; //paise
        this.options.prefill.name = this.User.name;
        this.options.prefill.email = this.User.email;
        this.options.prefill.contact = this.User.mobileno;
        
        if(data.pgName ==='razor1') {
          this.options.image="";
          var rzp1 = new Razorpay(this.options);
          rzp1.open();
        }
       
                
        rzp1.on('payment.failed', function (response:any){    
          // Todo - store this information in the server
          console.log(response);
          console.log(response.error.code);    
          console.log(response.error.description);    
          console.log(response.error.source);    
          console.log(response.error.step);    
          console.log(response.error.reason);    
          console.log(response.error.metadata.order_id);    
          console.log(response.error.metadata.payment_id);
        }
        );
      }
      ,
      err => {
        this.error = err.error.message;
      }
      );
    }

    @HostListener('window:payment.success', ['$event']) 
    onPaymentSuccess(event:any): void {
       console.log(event.detail);
    }
}