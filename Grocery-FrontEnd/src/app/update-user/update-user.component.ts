import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit
{
  User:any;
  
  constructor(private route:Router,private loginService:LoginService)
  {

  }

  ngOnInit(): void 
  {
    console.log(this.loginService.User)
    this.User=this.loginService.User;
  }

  updateUser(updateUserForm:any)
  {
    this.loginService.updateUser(updateUserForm).subscribe((data:any)=>{
      console.log(data);
      alert("Updated Successfully");
      this.route.navigateByUrl("user");
    });
  }
}
