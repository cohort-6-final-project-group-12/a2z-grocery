import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-productcategories',
  templateUrl: './productcategories.component.html',
  styleUrls: ['./productcategories.component.css']
})
export class ProductcategoriesComponent implements OnInit
{
  searchText="";
  ProductCategory:any[]=[];

  constructor(private route:Router, private productService:ProductService) 
  {
    this.getProductCategory();
  }
  
  ngOnInit(): void
  {
    this.getProductCategory();
  }

  addProductCat()
  {
    this.route.navigateByUrl("addPC");
  }

  updateProductCat(pc:any)
  {
    this.productService.pc=pc;
    this.route.navigateByUrl("updatePC");
  }

  getProductCategory()
  {
    return this.productService.getProductCategory().subscribe((data:any)=>{
      console.log(data);
      this.ProductCategory=Object.values(data);
    });
  }
}
