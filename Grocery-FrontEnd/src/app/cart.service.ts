import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService 
{
  cartItems:any=[];

  constructor(private httpclient: HttpClient) {}

  displayCartByUiD(uid:any) : Observable<any>
  {
    this.cartItems=this.httpclient.get("/Cart/"+uid);
    return this.cartItems;
  }

  updateCart(x:any) {
    return this.httpclient.put("/displayCart",x);
  }
  
  displayCart() {
    return this.httpclient.get("/displayCart");
  }
  
  addtoCart(c: any) {
    return this.httpclient.post("/displayCart", c);
  }
  
  deleteiteminCart(cid: any) {
    return this.httpclient.delete("/displayCart/" + cid);
  }

  deleteiteminCartByUid(uid:any){
    return this.httpclient.delete("/displayCartByUid/" + uid);
  }
}
